module.exports = {
  HOST: "localhost",
  USER: "sistemasPosgrado",
  PASSWORD: "sistemas_posgrado@123",
  DB: "sistemasPosgrado",
  dialect: "mysql",
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
};